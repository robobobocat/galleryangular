import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { GalleryComponent } from './gallery/gallery.component';
import { LoginComponent } from './login/login.component';
import { GalleryListComponent } from './gallery/gallery-list/gallery-list.component';
import { GalleryDetailsComponent } from './gallery/gallery-details/gallery-details.component';
import { GalleryListItemComponent } from './gallery/gallery-list/gallery-list-item/gallery-list-item.component';
import { AuthService } from './services/auth.service';
import { GalleryService } from './services/gallery.service';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    GalleryComponent,
    LoginComponent,
    GalleryListComponent,
    GalleryDetailsComponent,
    GalleryListItemComponent,
    ProfileComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    AuthService,
    GalleryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
